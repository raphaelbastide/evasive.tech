var d = document,
b = d.body,
sun = d.querySelector('.sun')

var angle,x,y,s;
function ellipse(w,h,i){
  angle = Math.random() * (Math.PI * 2);
  x = Math.round(Math.cos(angle) * w);
  y = Math.round(Math.sin(angle) * Math.random()*h);
  s = Math.floor(i*.007 + 1)
  var l = d.createElement('div')
  l.classList.add('dot')
  l.setAttribute('style', 'left:'+x+'px; top:'+y+'px; width:'+s+'px; height:'+s+'px;')
  sun.appendChild(l)
  l.onclick = function(){up()}
}
var limit = 2400
var i = 0
var draw = setInterval(function () {
  if (i < limit) {
    ellipse(240,200,i)
  if (i == limit/2) {
    b.classList.add('light')
  }
  }else {
    clearInterval(draw)
    up()
  }
  i++
}, 10);

function up(){
  var px = d.querySelectorAll('.dot')
  var j = 0
  var moveUp = setInterval(function () {
    if (j < px.length) {
      // var topPos = px[j].style.top - 20
      var topPos = parseInt(px[j].style.top, 10);
      topPos = topPos - 10
      px[j].style.top = topPos+"px"
    }else {
      clearInterval(moveUp)
      up()
    }
    j++
  }, 5);
}
