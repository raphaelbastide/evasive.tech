cv = document.querySelector("#cv");
img = document.querySelector("#img");
out = document.querySelector("#out");

function loaded() {
  c = cv.getContext("2d");
  c.drawImage(img, 0,0,92,42);
  var imgData = c.getImageData(0, 0, 92, 42);
  // get color pixels rgba
  var count=1;
  var pxw=4;
  for (var i = 0; i < imgData.data.length; i += 4) {
    var px = document.createElement("div"),
    r = imgData.data[i],
    g = imgData.data[i+1],
    b = imgData.data[i+2],
    rgb = r+','+g+','+b
    px.setAttribute('style','background:rgba('+rgb+'); width:'+pxw+'px; height:'+pxw+'px;')
    out.appendChild(px)
  }
}

if (img.complete) {
  loaded()
} else {
  img.addEventListener('load', loaded)
  img.addEventListener('error', function() {
    alert('error')
  })
}
