var d = document,
b = d.body
var offset = 5
var xoffset =  45
var yoffset = 2
var timeline = d.querySelector('.timeline input')
var jumper = d.querySelector('.jumper')
var facecontainer = d.querySelector('.facecontainer')
createDots()

function createDots(){
  for (var i = 0; i < c1.length; i++) {
    var x1 =  c1[i][0] + (4*offset) + xoffset
    var y1 =  c1[i][1] + (0*offset) * yoffset
    var dot = d.createElement('div')
    dot.classList.add('dot')
    dot.setAttribute('style','top:'+y1+'%; left:'+x1+'%;')
    jumper.appendChild(dot)
  }
}
function moveDots(step){
  var dots = d.querySelectorAll('.dot')
  for (var i = 0; i < c1.length; i++) {
    if (step == 1) {
      var x =  c1[i][0] + (4*offset) + xoffset
      var y =  c1[i][1] + (0*offset) * yoffset
    }else if (step == 2) {
      var x =  c2[i][0] + (3*offset) + xoffset
      var y =  c2[i][1] + (1*offset) * yoffset
    }else if (step == 3) {
      var x =  c3[i][0] + (2*offset) + xoffset
      var y =  c3[i][1] + (2*offset) * yoffset
    }else if (step == 4){
      var x =  c4[i][0] + (1*offset) + xoffset
      var y =  c4[i][1] + (3*offset) * yoffset
    }else {
      var x =  c5[i][0] + (0*offset) + xoffset
      var y =  c5[i][1] + (4*offset) * yoffset
    }
    var transitionDelay = i * .01
    dots[i].setAttribute('style','top:'+y+'%; left:'+x+'%; transition-delay:'+transitionDelay+'s;')
  }
}

// function moveDot(item, x, y, i, delay){
//   delay = delay * 1000
//   var transitionDelay = i * .0001
//   setTimeout(function () {
//     item.setAttribute('style','top:'+y+'px; left:'+x+'px; transition-delay:'+transitionDelay+'s;')
//   }, delay);
// }

timeline.onchange = function(){
  var val = this.value
  moveDots(val)
  updateFace(val)
}
drawFace()

function updateFace(val){
  var dots = d.querySelectorAll('.facedot')
  var s = (val - 1) * 3
  for (var i = 0; i < dots.length; i++) {
    dots[i].style.width = s+'px'
    dots[i].style.height = s+'px'
  }
}
function drawFace(){
  for (var i = 0; i < face.length; i++) {
    var x =  face[i][0] * 2 - 5
    var y =  face[i][1] * 2 + 30
    var dot = d.createElement('div')
    dot.classList.add('facedot')
    dot.setAttribute('style','top:'+y+'%; left:'+x+'%;')
    facecontainer.appendChild(dot)
  }
}
