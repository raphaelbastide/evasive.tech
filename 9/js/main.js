var d = document,
b = d.body

// 1
cv = document.querySelector("#cv");
img = document.querySelector("#i1");
out = document.querySelector("#out");

function loaded() {
  c = cv.getContext("2d");
  c.drawImage(img, 0,0,9,8);
  var imgData = c.getImageData(0, 0, 9, 8);
  // get color pixels rgba
  var count=1;
  var pxw=4;
  for (var i = 0; i < imgData.data.length; i += 4) {
    var px = document.createElement("div"),
    r = imgData.data[i],
    g = imgData.data[i+1],
    b = imgData.data[i+2];
    if (r == 255 && g == 255 && b == 255) {
      var rgba = r+','+g+','+b+',0'
    }else {
      var rgba = r+','+g+','+b+',1'
    }
    px.setAttribute('style','background:rgba('+rgba+'); width:'+pxw+'px; height:'+pxw+'px;')
    out.appendChild(px)
  }
}

if (img.complete) {
  loaded()
} else {
  img.addEventListener('load', loaded)
  img.addEventListener('error', function() {
    alert('error')
  })
}

// 5
var fog = d.querySelector('.fog')
var angles = ['0', '-45', '45']
function createFogItem(a1, a2){
  var angle = angles[Math.floor(Math.random()* angles.length)]
  var item = d.createElement("div")
  item.classList.add('fog-item')
  item.setAttribute('style', 'background:linear-gradient('+angle+'deg, rgba(0, 0, 0, '+a1+') 0%, rgba(0, 0, 0, '+a2+') 100%);')
  fog.appendChild(item)
}
setInterval(function () {
  var a1 = 0
  var a2 = 1
  fog.innerHTML = ""
  createFogItem(a1, a2)
}, 20);


var rockbox = d.querySelector('.rock-box')
var rockNbr = 0
setInterval(function(){
  var r = Math.round(Math.random() * 10) + 3
  if (rockNbr >= r) {
    rockNbr = 0
    rockbox.innerHTML = ""
  }else {
    createRock()
    rockNbr++
  }
}, 500)

function createRock(){
  var rock = d.createElement("div")
  rock.classList.add('rock')
  // rock.setAttribute('style', 'transform:skew('+t1+'deg, 0deg);')
  rockbox.appendChild(rock)
}

// 7
var branchBox = d.querySelector('.mecha-7')
var angle = 0
var nbrBranch = 90
for (var i = 0; i < nbrBranch; i++) {
  createBranch()
}

function createBranch(){
  var branch = d.createElement('div')
  branch.classList.add('branch')
  branch.setAttribute('style', 'transform:rotate('+angle+'deg);')
  branchBox.appendChild(branch)
  angle+=4
}

function is_touch_device() {
  var prefixes = ' -webkit- -moz- -o- -ms- '.split(' ');
  var mq = function (query) {
      return window.matchMedia(query).matches;
  }
  if (('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch) {
      return true;
  }
  var query = ['(', prefixes.join('touch-enabled),('), 'heartz', ')'].join('');
  return mq(query);
}

function addBranch(nbr){
  var branches = d.querySelectorAll('.branch:not(.visible)')
  for (var i = 0; i < nbr; i++) {
    var r = Math.floor(Math.random() * branches.length)
    branches[r].classList.add('visible')
  }
}

if (is_touch_device) {
  d.onmousemove = function(){
    addBranch(1)
  };
}else {
  b.onclick = function(){
    addBranch(8)
  }
}
