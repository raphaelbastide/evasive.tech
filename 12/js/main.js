var d = document,
b = d.body


let c = document.getElementById('c');
let ctx = c.getContext('2d');
let size = { x: c.width, y: c.height };
console.log(size);
let definition = 180;
let partSize = size.y / definition;
let source = 'img/face.png'
let picData = [];
let points = [];

async function load(data) {
	if (data instanceof File) {
		return new Promise((res, rej) => {
			let reader = new FileReader();
			reader.onload = () => res(load(reader.result));
			reader.onerror = rej;
			reader.readAsDataURL(data);
		});
	}
	return new Promise((res, rej) => {
		let img = new Image();
		img.onload = () => res(img);
		img.onerror = rej;
		img.crossOrigin = 'anonymous';
		img.src = data;
	});
}
function getLightData(image, size) {
	if (!size) {
		size = { x: img.width, y: img.height };
	}
	let c = document.createElement('canvas');
	let ctx = c.getContext('2d');
	c.width = size.x;
	c.height = size.y;
	ctx.fillRect(0, 0, c.width, c.height);
	let w = size.x;
	let h = size.y;
	let imgRatio = image.width / image.height;
	let finalRatio = w / h;
	if (imgRatio > finalRatio) {
		w = h * imgRatio;
	} else {
		h = w / imgRatio;
	}
	ctx.drawImage(image, (size.x - w) * .5, (size.y - h) * .5, w, h);
	let data = ctx.getImageData(0, 0, size.x, size.y).data;
	let ret = [];
	for (let i = 0, n = data.length; i < n; i += 4) {
		ret.push((.2126 * data[i] + .7152 * data[i + 1] + .0722 * data[i + 2]) / 255);
	}
	return ret;
}
let currentImage = null;
async function prepare(source) {
	if (source === currentImage) {
		return;
	}
	currentImage = source;
	picData = [];
	let image = await load(source);
	if (currentImage !== source) {
		return;
	}
	picData = getLightData(image, size);
}

function at(x, y) {
	return picData[~~x + ~~y * size.y] || 0;
}
function addLine() {
	points.push(...[...Array(definition)].map((e, i) => ({ y:size.y, x: i * (partSize + 10) })));
}

let spawner = (() => {
	let last = 0;
	// interval in seconds
	let interval = .3;
	return {
		setInterval: v => interval = v || 1,
		tick: dt => {
			last += dt || 0;
			if (last >= interval) {
				addLine();
				last = last % interval;
			}
		}
	};
})();

// let speed = 100;
let speed = 100;
let minSpeed = .05;

// Under 30fps, prefer lagging than time accuracy
let maxDT = 1 / 30;
let previous;
function loop(timestamp) {
	if (!previous) {
		previous = timestamp;
	}
	let dt = (timestamp - previous) * .001;
	if (dt > maxDT) {
		dt = maxDT;
	}
	previous = timestamp;
	spawner.tick(dt);
	ctx.fillStyle = '#111';
	ctx.fillRect(0, 0, size.x, size.y);
	ctx.fillStyle = 'yellow';
	for (let i = points.length; i--;) {
		var r = Math.round(Math.random()*7)-7
		let point = points[i];
		let val = 1 - at(point.x, point.y);
		let factor = minSpeed + (1 - minSpeed) * (val * val);
		point.y -= speed * dt * factor;
		if (point.x >= size.x || point.x < 0 || point.y >= size.y || point.y < 0 || point.x <= 295 || point.x >= 713) { points.splice(i, 1);
			continue;
		}
		ctx.fillRect(point.x, point.y , 2, 2);
	}
	requestAnimationFrame(loop);
}


function checkSize() {
	let size = Math.min(window.innerWidth, window.innerHeight, c.width);
	c.style.width = size + 'px';
	c.style.height = size + 'px';
}
window.addEventListener('resize', checkSize);

checkSize();
prepare(source).then(() => requestAnimationFrame(loop));
