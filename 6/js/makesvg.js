// https://codepen.io/vvanghelue/pen/XPbwmj

const generatePath = (width = 400, totalPoints = 20, rounded = 0.2, nocurve = true) => {
  const center = [width/2, width/2]
  const points = []
  let angle = 0
  for (let i = 0; i < totalPoints; i++) {
    angle += 2 * Math.PI/totalPoints
    let randomRadius = (Math.random() * .1 + .4 ) * width / 3.4
    let point = [
      center[0] + randomRadius * Math.cos(angle),
      center[1] + randomRadius * Math.sin(angle) /// 1.3 // give a ciruit shape
    ]

    let bezierAngle = angle - 2 * Math.PI/(2 * totalPoints)
      + .3 * (Math.random() - .5)
    let bezierRadius = randomRadius * (1 + rounded * (Math.random() - 1))
    let bezierPoint1 = [
      center[0] + bezierRadius * Math.cos(bezierAngle),
      center[1] + bezierRadius * Math.sin(bezierAngle)
    ]
    let bezierPoint2 = [
      point[0] - (bezierPoint1[0] - point[0]),
      point[1] - (bezierPoint1[1] - point[1]),
    ]
    points.push({
      point: point,
      bezier: [
        bezierPoint1,
        bezierPoint2
      ]
    })
  }

  // nocurve
  if (nocurve) {
    points.forEach((p, k) => {
      if (k == 0) {
        points[0].bezier = [
          points[points.length - 1].point,
          points[1].point
        ]
        return
      }
      if (k == (points.length - 1)) {
        points[k].bezier = [
          points[k - 1].point,
          points[0].point
        ]
        return
      }
      points[k].bezier = [
        points[k - 1].point,
        points[k + 1].point
      ]
    })
  }
  return points
}

const randomCircuitGenerator = (id) => {
  const pathPoints = generatePath()
  let pathData = ``
  pathPoints.forEach((p, i) => {
    //first
    if (i == 0) {
      pathData += `
        M ${pathPoints[i].point[0]},${pathPoints[i].point[1]}
      `
      return
    }
    //last
    if (i == pathPoints.length - 1) {
      pathData += `
        C ${pathPoints[i-1].bezier[1][0]},${pathPoints[i-1].bezier[1][1]}
          ${pathPoints[i].bezier[0][0]},${pathPoints[i].bezier[0][1]}
          ${pathPoints[i].point[0]},${pathPoints[i].point[1]}

        C ${pathPoints[i].bezier[1][0]},${pathPoints[i].bezier[1][1]}
          ${pathPoints[0].bezier[0][0]},${pathPoints[0].bezier[0][1]}
          ${pathPoints[0].point[0]},${pathPoints[0].point[1]}
      `
      return
    }
    //all middle points
    pathData += `
        C ${pathPoints[i-1].bezier[1][0]},${pathPoints[i-1].bezier[1][1]}
          ${pathPoints[i].bezier[0][0]},${pathPoints[i].bezier[0][1]}
          ${pathPoints[i].point[0]},${pathPoints[i].point[1]}
    `
  })

  return `
    <svg class="glow" width="400" height="400" xmlns="http://www.w3.org/2000/svg">
      <path d="${pathData}"/>
    </svg>
    <svg class="main" fill="url(#fill-${id})" width="400" height="400" xmlns="http://www.w3.org/2000/svg">
      <path d="${pathData}"/>
    </svg>
    <svg class="shadow2" width="400" height="400" xmlns="http://www.w3.org/2000/svg">
      <path d="${pathData}"/>
    </svg>
  `
}
// const makeSVG = (id) => {
//   document.querySelector('.rocks').innerHTML += randomCircuitGenerator(id)
// }
