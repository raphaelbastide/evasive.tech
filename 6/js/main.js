d = document
b = document.body

var rocks = d.querySelector('.rocks')
var cols = ['coral','yellow','crimson', 'cyan', 'darkgray', 'honeydew', 'gold', 'maroon','navy','orangered','teal', 'CornflowerBlue', 'aquamarine']
rocks.onclick = function(event){
  var rock = d.createElement('div')
  var x = event.clientX
  var y = event.clientY
  var r = Math.floor(Math.random() * cols.length)
  var c = cols[r]
  rock.setAttribute('style','top:'+y+'px;left:'+x+'px; background:'+c+'; box-shadow: 0 0px 20px '+c+', inset 0 -5px 5px rgba(0, 0, 0, .3);')
  // rock.setAttribute('type','radio')
  // rock.setAttribute('checked', 'true')
  rock.classList.add('rock')
  rocks.appendChild(rock)
};
