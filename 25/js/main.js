var d = document,
b = d.body

var lines = d.querySelectorAll('.line')
var main = d.getElementsByTagName('main')[0]
var alphabet = 'abcdefghijklmnopqrstuvwxyz '.split('');

for (var i = 0; i < lines.length; i++) {
  var string = lines[i].textContent
  lines[i].innerHTML =""
  var charz = string.split('')
  for (var j = 0; j < charz.length; j++) {
    var s = d.createElement('select')
    var o = d.createElement('option')
    o.setAttribute('value', charz[j])
    o.setAttribute('selected', 'true')
    o.innerHTML = charz[j]
    s.appendChild(o)
    for (var l = 0; l < alphabet.length; l++) {
      var oa = d.createElement('option')
      oa.setAttribute('value', alphabet[l])
      oa.innerHTML = alphabet[l]
      s.appendChild(oa)
    }
    lines[i].appendChild(s)
  }
}

var imgs = d.querySelectorAll('.images svg');
var texture = d.querySelector('#texture feTurbulence');

// breath(texture)

var direction = 'pos'
function breath(texture) {
  var size = 1;
  var id = setInterval(anim, 1000);
  function anim() {
    if (size == 6) {
      direction = 'neg'
    }
    if (size == 1 && direction == 'neg') {
      direction = 'pos'
    }
    if (direction == 'pos') {
      size++;
    }else {
      size--
    }
    console.log(size);
    texture.setAttribute('numOctaves', size);
  }
}
