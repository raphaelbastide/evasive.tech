
let bugsNbr = 4
let width = 300;
let height = 300;
let buffer = height/3;
let segments = 9; // must be an odd number since we're 'curving'
let main = document.getElementsByTagName('main')[0];
let north = document.querySelector('.north')
let east = document.querySelector('.east')
let south = document.querySelector('.south')
let west = document.querySelector('.west')

for (var i = 0; i < bugsNbr; i++) {
  if (i >= 0 && i < bugsNbr / 4 * 1) {
    var container = north
  }else if (i >= bugsNbr /4 * 1 && i < bugsNbr /4  * 2  ) {
    var container = east
  }else if (i >= bugsNbr /4  * 2 && i < bugsNbr /4  * 3  ) {
    var container = south
  }else{
    var container = west
  }
  container.innerHTML +='<div class="bug"><svg class="svg-'+i+'" id="svg-'+i+'" width="300px" height="300px" viewBox="0 0 300 300"><path id="path-0-'+i+'" class="animate-stroke" d="M0,0 S 582.904,227.835 425.37,478.521 711.671,552.493 345.918,810.027"/></svg><svg class="svg-'+i+'" id="svg-'+i+'" width="300px" height="300px" viewBox="0 0 300 300"><path id="path-1-'+i+'" class="animate-stroke" d="M0,0 S 582.904,227.835 425.37,478.521 711.671,552.493 345.918,810.027"/></svg></svg><svg class="svg-'+i+'" id="svg-'+i+'" width="300px" height="300px" viewBox="0 0 300 300"><path id="path-2-'+i+'" class="animate-stroke" d="M0,0 S 582.904,227.835 425.37,478.521 711.671,552.493 345.918,810.027"/></svg></svg><svg class="svg-'+i+'" id="svg-'+i+'" width="300px" height="300px" viewBox="0 0 300 300"><path id="path-3-'+i+'" class="animate-stroke" d="M0,0 S 582.904,227.835 425.37,478.521 711.671,552.493 345.918,810.027"/></svg></div>';
  // main.innerHTML += '</div>'
  var svgs = d.querySelectorAll('.svg-'+i)
  for (var j = 0; j < svgs.length; j++) {
    var path = d.getElementById('path-'+j+'-'+i)
    generateLine(path);
    setInterval(() => {
      let length = path.getTotalLength();
      path.classList.remove('animate-stroke');
      path.style.strokeDashoffset = length;
      console.log('reset');
      path.classList.add('animate-stroke');

    }, 3000);
  }
}

function generateLine(path) {
  let xPoints = [];
  let yPoints = [];
  function randomize(minimum, maximum) {
    return (Math.floor(Math.random() * (maximum - minimum + 1) + minimum));
  }

  function sortNumber(a,b) {
    return a - b;
  }

  function buildPathPoints() {
    let pathD = `M${xPoints[0]},${yPoints[0]} S`;
    for (let i=1; i<yPoints.length; ++i) {
      pathD = pathD + ' ' + xPoints[i] + ',' + yPoints[i];
    }
    return pathD;
  }

  for (let i=0; i<segments; ++i) {
    yPoints.push(i*(height / (segments-1)));
    if (i == 0) {
      xPoints.push(0);
    }else{
      xPoints.push(randomize(buffer,width-buffer));
    }
  }

  xPoints[0] += 10;
  xPoints[(xPoints.length - 1)] -= 10;
  path.setAttribute('d', buildPathPoints());
  length = path.getTotalLength();
  path.style.strokeDasharray = length;
  path.style.strokeDashoffset = length;
}

var sizes = ['small','medium','large']
var cols = ['255,0,0', '0,255,0', '255,0,255', '255,255,0', '100, 100, 100', '255,255,255']

var createSpotInt = setInterval(function () {
  createSpot()
}, 3000);
function createSpot(){
  t = Math.round(Math.random() * 100)
  l = Math.round(Math.random() * 100)
  c = Math.floor(Math.random() * cols.length)
  s = Math.floor(Math.random() * sizes.length)
  col = cols[c]
  size = sizes[s]
  var spot = d.createElement('div')
  spot.classList.add('spot')
  spot.classList.add(size)
  spot.setAttribute('style','top:'+t+'%; left:'+l+'%; background: radial-gradient(ellipse at center, rgba('+col+',1) 0%, rgba('+col+',0) 50%);')
  main.appendChild(spot)
}

var frames = d.querySelectorAll('.frame')
var highlight = setInterval(function () {
  for (var i = 0; i < frames.length; i++) {
    frames[i].classList.remove('high')
  }
  f = Math.floor(Math.random() * frames.length)
  frames[f].classList.add('high')
}, 1500);

b.onclick = function(){
  createSpot()
}
