# evasive.tech

evasive.tech is a daily online artwork routine developed by Raphaël Bastide throughout the quarantine period (starting 17/03/2020, ending 21/04/2020). Like his previous web-based project otherti.me, evasive.tech is a series of episodes, each being envisioned and coded on a daily basis. evasive.tech will develop a narrative through composition and captions.

https://oo8.be/evasive.tech/
