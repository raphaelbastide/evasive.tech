var d = document,
b = d.body

window.onload=toBottom;

function toBottom(){
  window.scrollTo(0, document.body.scrollHeight);
}
function pageScroll() {
  window.scrollBy(0,-1);
  scrolldelay = setTimeout(pageScroll,100);
}
pageScroll()
var groups = d.querySelectorAll('.trees g')
var leaves = d.querySelectorAll('.leave')
setInterval(function(){
  for (var i = 0; i < groups.length; i++) {
    var t1 = Math.round(Math.random()* 2)
    var t2 = Math.round(Math.random()* 2)
    groups[i].setAttribute('style', 'transform:skew('+t1+'deg,'+t2+'deg);')  }
}, 7000)

setInterval(function(){
  for (var i = 0; i < leaves.length; i++) {
    var t1 = Math.round(Math.random()* 100) - 50
    var t2 = Math.round(Math.random()* 100) - 50
    leaves[i].setAttribute('style', 'transform:skew('+t1+'deg,'+t2+'deg);')
  }
}, 4000)

var bladebox = d.querySelector('.blade-box')
var bladeNbr = 0
setInterval(function(){
  if (bladeNbr >= 500) {
    bladeNbr = 0
    bladebox.innerHTML = ""
  }else {
    createBlade()
    bladeNbr++
  }
}, 500)

// var cols = ['#fff16f', '#aad993', '#ff8549', '#77f3ff', '#828282']
function createBlade(){
  var t1 = Math.round(Math.random()* 40) - 40
  var t2 = Math.round(Math.random()* 40) - 40
  var b = Math.round(Math.random()* 4)+2
  var blade = d.createElement("div")
  blade.setAttribute('style', 'transform:skew('+t1+'deg, 0deg);')
  bladebox.appendChild(blade)
}
