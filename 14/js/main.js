var d = document,
b = d.body,
main = d.getElementById('main')
var wW = window.innerWidth
var wH = window.innerHeight
var iW = 12
var iH = 12
var point = pointStart = 40
var limit = 500
var id = 0
var blinkI
var drawing = [236,217,211,192,167,173,148,104,79,85,60,98,92,86,105,149,193,237,91,97,103,147,191,235,66,54,35,41,29,23,17,10,2,16,22,4,11,5,3,9,2,8,13,7,1,6,12,19,32,51,76,120,164,208,57,38,44,26,25,50,31,94,138,182,226,18,20,14,252,270,69]
var si = setInterval(function () {
  var x, y
  x = Math.round(Math.cos(point) * 10 * point *.1)
  y = Math.round(Math.sin(point) * 10 * point *.1)
  point++
  if (point >= limit) {
    clearInterval(si)
  }else {
    placeItem(wW/2 + x ,wH/2 + y, iW, iH)
  }
}, 5);
placeItem(wW/2 ,wH/2, iW, iH)

function placeItem(x,y,w,h){
  var item = d.createElement('input')
  item.setAttribute('type', 'radio')
  item.setAttribute('checked', 'true')
  item.classList.add('pebble')
  item.setAttribute('id',id)
  if (drawing.includes(id)) {
    item.classList.add('drawing')
  }
  id++
  item.setAttribute('style', 'top:'+y+'px;left:'+x+'px; width:'+w+'px; height:'+h+'px;')
  main.appendChild(item)
  item.onclick = function(){
    console.log(this.id);
  }

  var start = setInterval(function () {
    var pebbles = d.querySelectorAll('.pebble')
    item.removeAttribute('checked')
    if (pebbles.length == limit - pointStart) {
      console.log('endstart');
      clearInterval(start)
    }
  }, 200);}

  var revealer = d.querySelectorAll('.revealer')[0]

var pebbles = d.querySelectorAll('.pebble')
pebbles[0].onclick = function(){
  blink(true, 300)
}
revealer.onmousedown = function(){
  var pebbles = d.querySelectorAll('.pebble')
  for (var i = 0; i < pebbles.length; i++) {
    pebbles[i].setAttribute('checked','true')
  }
}
revealer.onmouseup = function(){
  for (var i = 0; i < pebbles.length; i++) {
    pebbles[i].removeAttribute('checked')
  }
}

function blink(check, time){
  pebbles = d.querySelectorAll('.pebble')
  console.log('blink');
  var c = 0
  var u = 0
  var check = setInterval(function () {
    if (c < pebbles.length) {
      pebbles[c].setAttribute('checked','true')
    }else {
      clearInterval(check)
    }
    c++
  }, 10);
  var uncheck = setInterval(function () {
    if (u < pebbles.length) {
      pebbles[u].removeAttribute('checked')
    }else {
      clearInterval(uncheck)
    }
    u++
  }, 20);
}
