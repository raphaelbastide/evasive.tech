var d = document,
b = d.body
items = 30;
var fog = d.querySelector('.fog')
var angles = ['0', '-45', '45']
function createFogItem(a1, a2){
  var angle = angles[Math.floor(Math.random()* angles.length)]
  var item = d.createElement("div")
  item.classList.add('fog-item')
  item.setAttribute('style', 'background:linear-gradient('+angle+'deg, rgba(57, 75, 20, '+a1+') 0%, rgba(57, 75, 20, '+a2+') 100%);')
  fog.appendChild(item)
}

var max = 100
setInterval(function () {
  var a1 = 0
  var a2 = 0
  fog.innerHTML = ""
  for (var i = 0; i < max; i++) {
    a2 = i / max
    a1 = a1 + a2 / 2
    createFogItem(a1, a2)
  }

}, 20);

// for (var i = 0; i < max; i++) {
//   a2 = i / max
//   a1 = a1 + a2 / 2
//   createFogItem(a1, a2)
// }
