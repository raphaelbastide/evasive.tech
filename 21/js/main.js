/*
	Three.js "tutorials by example"
	Author: Lee Stemkoski
	Date: July 2013 (three.js v59dev)
*/

// MAIN

// standard global variables
var container, scene, camera, renderer, controls, stats;
var keyboard = new THREEx.KeyboardState();
var clock = new THREE.Clock();
// custom global variables
var cube, object
var windowWidth, windowHeight;
var views = [
      {
        left: 0,
        bottom: 0,
        width: 0.5,
        height: 1.0,
        background: new THREE.Color( 0, 0, 0 ),
        eye: [ 0, 100, 100 ],
        up: [ 0, 1, 0 ],
        fov: 10,
        updateCamera: function ( camera, scene) {
          camera.position.set(0,0,200);
          camera.lookAt( scene.position );
        }
      },
      {
        left: 0.5,
        bottom: 0,
        width: 0.5,
        height: 0.7,
        background: new THREE.Color( 0, 0, 1 ),
        eye: [ 00, 00, 0 ],
        up: [ 0, 1, 0 ],
        fov: 5,
        updateCamera: function ( camera, scene) {
          camera.position.set(-1000,00,-100);
          camera.lookAt( 0,40,0 );
        }
      },
      {
        left: 0.5,
        bottom: 0.7,
        width: 0.5,
        height: 0.3,
        background: new THREE.Color( 1, 1, 1 ),
        eye: [ 1, 0, 00 ],
        up: [ 0, 1, 0 ],
        fov: 50,
        updateCamera: function ( camera, scene) {
          camera.position.set(30,-0,300);
          camera.lookAt( 0,440,0 );
        }
      }
    ];
init();
animate();

// FUNCTIONS
function init()
{
	// SCENE
	scene = new THREE.Scene();
	// CAMERA
	var SCREEN_WIDTH = window.innerWidth, SCREEN_HEIGHT = window.innerHeight;
	// var VIEW_ANGLE = 15, ASPECT = SCREEN_WIDTH / SCREEN_HEIGHT, NEAR = 0.1, FAR = 20000;
	// camera = new THREE.PerspectiveCamera( VIEW_ANGLE, ASPECT, NEAR, FAR);
	// scene.add(camera);
	// camera.position.set(00,-100,100);
  // camera.lookAt(scene.position);
  // camera.lookAt(0,200,400);



	// RENDERER
	if ( Detector.webgl )
    renderer = new THREE.WebGLRenderer( { antialias: true } );
	else
		renderer = new THREE.CanvasRenderer();
	renderer.setSize(SCREEN_WIDTH, SCREEN_HEIGHT);
	container = document.getElementById( 'canvas' );
	container.appendChild( renderer.domElement );
	// EVENTS
	THREEx.WindowResize(renderer, camera);
	THREEx.FullScreen.bindKey({ charCode : 'm'.charCodeAt(0) });
	// CONTROLS

  for ( var ii = 0; ii < views.length; ++ ii ) {

    var view = views[ ii ];
    var camera = new THREE.PerspectiveCamera( view.fov, window.innerWidth / window.innerHeight, 1, 10000 );
    camera.position.fromArray( view.eye );
    camera.up.fromArray( view.up );
    view.camera = camera;

    view.updateCamera( camera, scene );

  }
  controls = new THREE.OrbitControls( camera, renderer.domElement );


	// STATS
	// stats = new Stats();
	// stats.domElement.style.position = 'absolute';
	// stats.domElement.style.bottom = '0px';
	// stats.domElement.style.zIndex = 100;
	// container.appendChild( stats.domElement );


  var billboardMaterial = new THREE.MeshBasicMaterial( { color: 0xffFF00, wireframe: false } )
	var billboardGeometry = new THREE.CubeGeometry( 800, 800, 4, 1, 1, 1 );
	var billboard = new THREE.Mesh(billboardGeometry, billboardMaterial);
	billboard.position.y = -0.5;
  billboard.position.set(393, 525, 0);

	// billboard.rotation.x = Math.PI / 2;
	scene.add(billboard);
	// SKYBOX/FOG
	// var skyBoxGeometry = new THREE.CubeGeometry( 10000, 10000, 10000 );
	// var skyBoxMaterial = new THREE.MeshBasicMaterial( { color: 0x9999ff, side: THREE.BackSide } );
	// var skyBox = new THREE.Mesh( skyBoxGeometry, skyBoxMaterial );
	// scene.add(skyBox);
	// scene.fog = new THREE.FogExp2( 0xffffff, 0.00003 );

	////////////
	// CUSTOM //
	////////////

	//////////////////////////////////////////////////////////////////////

	// this material causes a mesh to use colors assigned to faces
	// var cubeMaterial = new THREE.MeshBasicMaterial({ color: 0xffffff, vertexColors: THREE.FaceColors } );

	// var cubeGeometry = new THREE.CubeGeometry( 80, 80, 80, 3, 3, 3 );
	// for ( var i = 0; i < cubeGeometry.faces.length; i++ )
	// {
	// 	face  = cubeGeometry.faces[ i ];
	// 	face.color.setRGB( Math.random(), Math.random(), Math.random() );
	// }
	// cube = new THREE.Mesh( cubeGeometry, cubeMaterial );
	// cube.position.set(200, 0, 0);
	// scene.add(cube);



  var sphere = new THREE.SphereBufferGeometry( 3, 16, 8 );
  light1 = new THREE.PointLight( 0xFF0000, 100, 300 );
  light2 = new THREE.PointLight( 0xFFFF00, 100, 300 );
  light3 = new THREE.PointLight( 0x0000ff, 100, 300 );
  light4 = new THREE.PointLight( 0xffffff, 20, 100 );
  light1.add( new THREE.Mesh( sphere, new THREE.MeshBasicMaterial( { color: 0xf43604 } ) ) );
  light2.add( new THREE.Mesh( sphere, new THREE.MeshBasicMaterial( { color: 0xe9f744 } ) ) );
  light3.add( new THREE.Mesh( sphere, new THREE.MeshBasicMaterial( { color: 0x0012ff } ) ) );
  light4.add( new THREE.Mesh( sphere, new THREE.MeshBasicMaterial( { color: 0xffffff } ) ) );
  scene.add(light1);
  scene.add(light2);
  scene.add(light3);
  // scene.add(light4);

  var mat =  new THREE.MeshLambertMaterial( { color: 0xffffff } )
  var matblack =  new THREE.MeshLambertMaterial( { color: 0x000000 } )
  // obj.scale.set(1,1,1);
  var objectLoader = new THREE.ObjectLoader();
	objectLoader.load( "js/frog.json", function ( obj ) {
    obj.material = mat
	 	scene.add( obj );
	} );
  var objectLoader = new THREE.ObjectLoader();
	objectLoader.load( "js/eye1.json", function ( obj ) {
    obj.material = matblack
	 	scene.add( obj );
	} );
  var objectLoader = new THREE.ObjectLoader();
	objectLoader.load( "js/eye2.json", function ( obj ) {
    obj.material = matblack
	 	scene.add( obj );
	} );
  var objectLoader = new THREE.ObjectLoader();
	objectLoader.load( "js/tube1.json", function ( obj ) {
    obj.material = matblack
	 	scene.add( obj );
	} );
  var objectLoader = new THREE.ObjectLoader();
	objectLoader.load( "js/tube2.json", function ( obj ) {
    obj.material = matblack
	 	scene.add( obj );
	} );

  //
	// var ambientLight = new THREE.AmbientLight(0x333333);
	// scene.add(ambientLight);

}

function animate()
{
  requestAnimationFrame( animate );
	render();
	update();
}

function update(){

	controls.update();
}

function updateSize() {
  if ( windowWidth != window.innerWidth || windowHeight != window.innerHeight ) {
    windowWidth = window.innerWidth;
    windowHeight = window.innerHeight;
    renderer.setSize( windowWidth, windowHeight );
  }
}

function render()
{
  var time = Date.now()* (0.0005);
  var delta = clock.getDelta();

  if ( object ) object.rotation.y -= 0.5 * delta;

  updateSize();

  for ( var ii = 0; ii < views.length; ++ ii ) {

    var view = views[ ii ];
    var camera = view.camera;
    // view.updateCamera( camera, scene );

    // view.updateCamera( camera, scene, mouseX, mouseY );
    var left = Math.floor( windowWidth * view.left );
    var bottom = Math.floor( windowHeight * view.bottom );
    var width = Math.floor( windowWidth * view.width );
    var height = Math.floor( windowHeight * view.height );

    renderer.setViewport( left, bottom, width, height );
    renderer.setScissor( left, bottom, width, height );
    renderer.setScissorTest( true );
    renderer.setClearColor( view.background );

    light1.position.x = Math.sin( time * 0.7 ) * 130;
    light1.position.y = Math.cos( time * 0.5 ) * 30;
    light1.position.z = Math.cos( time * 0.3 ) * 10;
    light2.position.x = Math.sin( time * 0.7 ) * 100;
    light2.position.y = Math.cos( time * 0.2 ) * 130;
    light2.position.z = Math.cos( time * 0.5 ) * 10;
    light3.position.x = Math.sin( time * 0.3 ) * 130;
    light3.position.y = Math.cos( time * 0.2 ) * 30;
    light3.position.z = Math.cos( time * 0.7 ) * 100;
    light4.position.x = Math.sin( time * 0.3 ) * 30;
    light4.position.y = Math.cos( time * 0.2 ) * 30;
    light4.position.z = Math.cos( time * 0.7 ) * 100;
  	// renderer.render( scene, camera );


    // camera.aspect = width / height;
    // camera.updateProjectionMatrix();


    renderer.render( scene, camera );
  }
}
