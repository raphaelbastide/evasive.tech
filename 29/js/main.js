var d = document,
b = d.body

var inputs = d.querySelectorAll('.tree input')

for (var i = 0; i < inputs.length; i++) {
  var w = (i + 1) * 50
  var v = w * .5

  // console.log(i, w, v);
  inputs[i].style.width = w+'px'
  inputs[i].setAttribute('max', w)
  inputs[i].setAttribute('value', v)
  inputs[i].onchange = function(){
    changeW(this, i)
  }

  var h = d.createElement('div')
  h.classList.add('head')
  inputs[i].parentNode.appendChild(h)
  for (var j = 0; j < (i+1)*100; j++) {
    ellipse(w/2,w/2,i*3,h)
  }
  changeW(inputs[i], i)
}

function changeW(item, index){
  var val = item.value * 2
  var d = item.value / 1.2
  var container = item.parentNode.parentNode.parentNode;
  var inputcontainer = item.parentNode;
  var head = item.parentNode.childNodes[1];
  var valhead = val
  inputcontainer.style.animationDuration = d+'s';
  container.setAttribute('style','width:'+val+'px; height:'+val+'px;')
  head.setAttribute('style','left:'+valhead+'px;')
}

function ellipse(w,h,i,c){
  angle = Math.random() * (Math.PI * 2);
  x = Math.round(Math.cos(angle) * w);
  y = Math.round(Math.sin(angle) * Math.random()*h);
  s = Math.floor(i + 2)
  var l = d.createElement('div')
  l.classList.add('dot')
  l.setAttribute('style', 'left:'+x+'px; top:'+y+'px; width:'+s+'px; height:'+s+'px;')
  c.appendChild(l)
}
