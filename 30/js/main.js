var d = document,
b = d.body,
c1 = d.querySelector('.c1')
c2 = d.querySelector('.c2')
map = d.querySelector('.map')

for (var i = 0; i < 15; i++) {
  var bp = d.createElement('div')
  bp.classList.add('part')
  w = i * 14
  h = i * 4
  l = -w / 2 + 100 // 100 = .5* parent width
  t = i * 2 + 100
  delay = -i * .07
  // bp.setAttribute('style','width:'+w+'px; height:'+h+'px; margin-top:'+t+'px; left:'+w+'px; animation-delay:'+delay+'s;')
  bp.setAttribute('style','width:'+w+'px; height:'+h+'px; top:'+t+'px; left:'+l+'px; animation-delay:'+delay+'s;  animation-duration:2s;')
  var binbr = 9 // odd
  var inc = 0
  for (var j = 0; j < binbr; j++) {
    var bi = d.createElement('div')
    bi.classList.add('body-item')
    w2 = .7 * i
    t2 = j * 2
    if (j >= binbr / 2) {inc--}else {inc++}
    delay2 = .2 * inc
    h2 =  20
    bi.setAttribute('style','width:'+w2+'px; height:'+h2+'px; animation-delay:'+delay2+'s; animation-duration:2s;')
    bp.appendChild(bi)
  }
  c1.appendChild(bp)
}

for (var i = 0; i < 15; i++) {
  var bp = d.createElement('div')
  bp.classList.add('part')
  w = i * 20
  h = i * 8
  l = -w / 2 + 100 // 100 = .5* parent width
  t = i * 1 + 80
  delay = -i * .5
  bp.setAttribute('style','width:'+w+'px; height:'+h+'px; margin-top:'+t+'px; left:'+w+'px; animation-delay:'+delay+'s; animation-duration:5s;')
  // bp.setAttribute('style','width:'+w+'px; height:'+h+'px; top:'+t+'px; left:'+l+'px; animation-delay:'+delay+'s;')
  var binbr = 3 // odd
  var inc = 0
  for (var j = 0; j < binbr; j++) {
    var bi = d.createElement('div')
    bi.classList.add('body-item')
    w2 = .7 * i
    t2 = j * 2
    if (j >= binbr / 2) {inc--}else {inc++}
    delay2 = .2 * inc
    h2 =  7
    bi.setAttribute('style','width:'+w2+'px; height:'+h2+'px; animation-delay:'+delay2+'s;  animation-duration:5s;')
    bp.appendChild(bi)
  }
  c2.appendChild(bp)
}
map.onclick = function(){
  if (b.classList.contains('details')) {
    b.classList.remove('details')
  }else {
    b.classList.add('details')
  }
}
