var d = document,
b = d.body,
billboard = d.querySelector('.billboard'),
table = d.querySelector('.billboard table'),
zoom = d.querySelector('.zoom')

var blocks = d.querySelectorAll('td')
for (var i = 0; i < blocks.length; i++) {
  var col = blocks[i].getAttribute('bgcolor')
  if (col == '#000000') {
    blocks[i].setAttribute('style','--index:'+i+';')
  }
  blocks[i].onmouseover = function(){
    console.log(this);
    var x = Math.round(Math.random()*100)-50
    var y = Math.round(Math.random()*100)-50
    var that = this
    that.setAttribute('style','top:'+y+'px; left:'+x+'px;')
    setTimeout(function () {
      that.setAttribute('style','top:0;left:0;')
    }, 400);
  }
}

var currentSize = table.clientHeight;
zoom.onchange = function(){
  var val = this.value
  var newSize = currentSize * val
  table.setAttribute('style', 'height:'+newSize+'px;')
}
