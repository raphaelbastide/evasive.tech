var d = document,
b = d.body
var trees = d.querySelector('.trees')
function growTree(x,y,angle,size,gen, bush){
  if (gen >= size) {
    return
  }else if (gen == 0) {
    var trunk = d.createElement('div')
    trunk.classList.add('trunk')
    var trunkH = size * 1.5
    var trunkW = Math.round(size/10)
    var trunkX = x - (trunkW/2)
    trunk.setAttribute('style','bottom:'+y+'px; left:'+trunkX+'px; height:'+trunkH+'px; transform:rotate('+angle+'deg); width:'+trunkW+'px;')
    y += trunkH - (trunkH/2)
    trees.appendChild(trunk)
  }else {
    var branch = d.createElement('div')
    branch.classList.add('branch')
    var r = Math.floor(Math.random()*size + angle*size*gen/500) - (size/2)
    rx = x+r*gen/50
    y += size/50
    h = size/4 + gen*2
    branch.setAttribute('style','bottom:'+y+'px; left:'+rx+'px; height:'+h+'px; transform:rotate('+r+'deg);')
    // angle = ra
    trees.appendChild(branch)
    // size = size + 1
  }
  gen++
  growTree(x, y, angle, size, gen, bush)
}

growTree(0,  0, -5, 70, 0)
growTree(100,  0, -5, 50, 0)
growTree(180, 0, -5, 30, 0)
growTree(250, 0, -5, 20, 0)
growTree(300, 0, -5, 15, 0)

trees.onclick = function(){
  this.innerHTML =""
  growTree(0,  0, -5, 70, 0)
  growTree(100,  0, -5, 50, 0)
  growTree(180, 0, -5, 30, 0)
  growTree(250, 0, -5, 20, 0)
  growTree(300, 0, -5, 15, 0)
}
