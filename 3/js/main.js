var d = document,
b = d.body

var btns = d.querySelectorAll('footer button')
for (var i = 0; i < btns.length; i++) {
  btns[i].onclick = function(){
    var tree = "tree-"+this.getAttribute('data-tree')
    b.setAttribute('class','')
    b.classList.add(tree)
  };
}

var wrap = d.getElementsByClassName("orbitbinder")[0];
var begX, begY, prevX, prevY, newX, newY, dv;
newX=13; newY=67;
d.onmousedown = beginD;
d.onmouseup = stopD;

function beginD(e){
  dv = wrap.parentNode.offsetWidth >> 8;
  begX = e.clientX/(dv!=0?dv:dv=1);
  begY = e.clientY/dv;
  prevX = newX;
  prevY = newY;
  drag = true;
  d.onmousemove = letsD;
  return false;
}

function letsD(e) {
  if (!drag) return;
  newX = (prevY > 0 && prevY < 180) ? (prevX - (e.clientX/dv) + begX) : (prevX + (e.clientX/dv) - begX);
  newY = prevY - (e.clientY /dv) + begY;
  wrap.style.transform = "rotateX(" + newY + "deg) rotateZ(" + newX + "deg)";
  return false;
}

function stopD() {
  drag = false;
  rotReset();
}

function rotReset() {
  if (newX >= 360 || newX < 0) newX -= 360 * Math.floor(newX / 360);
  if (newY >= 360 || newY < 0) newY -= 360 * Math.floor(newY / 360);
  wrap.style.transform = "rotateX(" + newY + "deg) rotateZ(" + newX + "deg)";
}

wrap.style.transform = "rotateX(" + (newY += .11) + "deg) rotateZ(" + (newX += .11) + "deg)";
