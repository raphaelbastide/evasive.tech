var d = document,
b = d.body,
main = d.querySelector('main')

var t = l = 0
function line(width){
  var total = width / 2
  var line = d.createElement('div')
  line.classList.add('line')
  for (var j = 0; j < total; j++) {
    var drop = d.createElement('div')
    var dropLine = d.createElement('div')
    drop.classList.add('drop')
    dropLine.classList.add('dropLine')
    drop.appendChild(dropLine)
    pos = - 100 * Math.floor(Math.random() * 6)
    l = Math.floor(Math.random() * 100)
    // pos = -600
    delay = j / total * 2
    drop.setAttribute('style', 'left:'+l+'%; animation-delay:'+delay+'s; background-position:'+pos+'px bottom;')
    l += 100 / total
    line.appendChild(drop)
  }
  line.setAttribute('style','width:'+width+'%; opacity:'+width/10+';')
  main.appendChild(line)
}
for (var i = 0; i < 40; i++) {
  line(1.2 * (i+1) + 10)
}

for (var j = 0; j < 20; j++) {
  var drop = d.createElement('div')
  drop.classList.add('bgdrop')
  l = Math.floor(Math.random() * 100)
  delay = j / 10
  drop.setAttribute('style', 'left:'+l+'%; animation-delay:'+delay+'s;')
  main.appendChild(drop)
}
