var d = document,
b = d.body

var m = d.getElementsByTagName('main')[0]
var branch = 100
var angle = 0
var nbrBranch = 360

for (var i = 0; i < nbrBranch; i++) {
  createBranch()
}

function createBranch(){
  var branch = d.createElement('div')
  branch.classList.add('branch')
  branch.setAttribute('style', 'transform:rotate('+angle+'deg);')
  m.appendChild(branch)
  angle++
}

function is_touch_device() {
  var prefixes = ' -webkit- -moz- -o- -ms- '.split(' ');
  var mq = function (query) {
      return window.matchMedia(query).matches;
  }
  if (('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch) {
      return true;
  }
  var query = ['(', prefixes.join('touch-enabled),('), 'heartz', ')'].join('');
  return mq(query);
}

var ladder = false
function addBranch(nbr){
  var branches = d.querySelectorAll('.branch:not(.visible)')
  if (!ladder) {
    for (var i = 0; i < nbr; i++) {
      if (branches.length > 0) {
        var r = Math.floor(Math.random() * branches.length)
        branches[r].classList.add('visible')
      }else {
        ladder = true
        console.log('ladder-on');
        b.classList.add('ladder-on')
      }
    }
  }
}

if (is_touch_device) {
  d.onmousemove = function(){
    addBranch(1)
  };
}else {
  b.onclick = function(){
    addBranch(8)
  }
}
