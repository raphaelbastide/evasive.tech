var d = document,
b = d.body,
main = d.querySelector('main'),
trees = d.querySelectorAll('.tree'),
t1 = d.querySelector('.t1'),
t2 = d.querySelector('.t2'),
t3 = d.querySelector('.t3'),
t4 = d.querySelector('.t4'),
t5 = d.querySelector('.t5'),
t6 = d.querySelector('.t6')
all = d.querySelector('.all')

function createhCell(tree, y=0, angle=0){
  var cell = d.createElement('div')
  var size = (tree * 15) - 5
  var duration = (tree * 4)
  var r = Math.round(Math.random()*3) + 1
  var bgname = tree+"-"+r
  cell.classList.add('cell')
  cell.setAttribute('style','width:100%; height:'+size+'px; animation: up '+duration+'s linear forwards, curve '+duration+'s ease-in-out; background-image:url(img/'+bgname+'.png);')
  d.querySelector('.t'+tree).appendChild(cell)
  if (tree !== 1) {
    setTimeout(function () {
      var clone = cell.cloneNode(true);
      d.querySelector('.b'+tree).appendChild(clone)
    }, tree * 2 * 1000);
  }
  setTimeout(function () {
    cell.remove()
  }, tree * 8000);
}

var st0, st1, st2, st3, st4, st5, st6

all.onclick = function(){
  var that = this
  if (!that.classList.contains('off')) {
    if (st0) clearTimeout(st0);
    createhCell(1)
    createhCell(2)
    createhCell(3)
    createhCell(4)
    createhCell(5)
    createhCell(6)
    that.classList.add('off')
    st0 = setTimeout(function() {
      that.classList.remove('off')
    }, 340);
  }
}
t1.onclick = function(){
  var that = this
  var index = that.getAttribute('data-tree')
  if (!that.classList.contains('off')) {
    if (st1) clearTimeout(st1);
    createhCell(index)
    that.classList.add('off')
    st1 = setTimeout(function() {
      that.classList.remove('off')
    }, 340);
  }
}
t2.onclick = function(){
  var that = this
  var index = that.getAttribute('data-tree')
  if (!that.classList.contains('off')) {
    if (st2) clearTimeout(st2);
    createhCell(index)
    that.classList.add('off')
    st2 = setTimeout(function() {
      that.classList.remove('off')
    }, 340);
  }
}
t3.onclick = function(){
  var that = this
  var index = that.getAttribute('data-tree')
  if (!that.classList.contains('off')) {
    if (st3) clearTimeout(st3);
    createhCell(index)
    that.classList.add('off')
    st3 = setTimeout(function() {
      that.classList.remove('off')
    }, 340);
  }
}
t4.onclick = function(){
  var that = this
  var index = that.getAttribute('data-tree')
  if (!that.classList.contains('off')) {
    if (st4) clearTimeout(st4);
    createhCell(index)
    that.classList.add('off')
    st4 = setTimeout(function() {
      that.classList.remove('off')
    }, 340);
  }
}
t5.onclick = function(){
  var that = this
  var index = that.getAttribute('data-tree')
  if (!that.classList.contains('off')) {
    if (st5) clearTimeout(st5);
    createhCell(index)
    that.classList.add('off')
    st5 = setTimeout(function() {
      that.classList.remove('off')
    }, 340);
  }
}
t6.onclick = function(){
  var that = this
  var index = that.getAttribute('data-tree')
  if (!that.classList.contains('off')) {
    if (st6) clearTimeout(st6);
    createhCell(index)
    that.classList.add('off')
    st6 = setTimeout(function() {
      that.classList.remove('off')
    }, 340);
  }
}
