var d = d,
b = d.body

var gameboard = d.getElementsByClassName('gameboard')[0];

var i, j;
var savedDesign = [];
var bw = 80
var bh = 20
var totalNodes = bw * bh
var gggInit = [24, 72, 74, 112, 113, 120, 121, 134, 135, 161, 165, 170, 171, 184, 185, 200, 201, 210, 216, 220, 221, 250, 251, 260, 264, 266, 267, 272, 274, 310, 316, 324, 361, 365, 412, 413];

Array.prototype.contains = function(obj) {
	if (this.indexOf(obj) !== -1) { return true; }
  else { return false; }
}

//
var getAdjacent = function(node) {
	var classes = node.className;
  var adjacents = [];
  var regx = /row(\d{1,2}) col(\d{1,2})/g;
  var matches = regx.exec(node.className);
  var myRow = matches[1];
  var myCol = matches[2];
  //Need 8 adjacents on central square
  adjacents.push(d.getElementsByClassName('row' + myRow + ' col' + (+myCol + 1))[0]);
  adjacents.push(d.getElementsByClassName('row' + myRow + ' col' + (+myCol - 1))[0]);
  adjacents.push(d.getElementsByClassName('row' + (+myRow + 1) + ' col' + myCol)[0]);
  adjacents.push(d.getElementsByClassName('row' + (+myRow - 1) + ' col' + myCol)[0]);
  adjacents.push(d.getElementsByClassName('row' + (+myRow + 1) + ' col' + (+myCol + 1))[0]);
  adjacents.push(d.getElementsByClassName('row' + (+myRow + 1) + ' col' + (+myCol - 1))[0]);
  adjacents.push(d.getElementsByClassName('row' + (+myRow - 1) + ' col' + (+myCol + 1))[0]);
  adjacents.push(d.getElementsByClassName('row' + (+myRow - 1) + ' col' + (+myCol - 1))[0]);
  return adjacents;
};

var toggleState = function(node) {
  var classes = node.className.split(' ');
  if (classes[0] !== "gameboard") {
  	if(node.className.match(/live/g)) {
      node.className = node.className.replace('live', 'dead');
    } else {
      node.className = node.className.replace('dead', 'live');
    }
  }
};

var evaluateNode = function(node) {
  var neighbors = getAdjacent(node);
  var thoseLeftStanding = 0;

  neighbors.forEach(function(adj) {
    if (!adj) { return; }
    if (adj.className.match(/(live)/g)) {
      thoseLeftStanding++;
    }
  });
  if (node.className.match(/live/g)) {
  	if (thoseLeftStanding < 2) { return 1; } // underpopulation
    if (thoseLeftStanding > 3) { return 1; } // overcrowding
  }
  if (node.className.match(/dead/g) && thoseLeftStanding === 3) { return 1; } // reproduction
  return 0;
};

var generation = function() {
  var flipMap = [];
  var nodes = d.getElementsByClassName('pixel');
  [].forEach.call(nodes, function(node){ flipMap.push(evaluateNode(node)); });
  [].forEach.call(nodes, function(node, idx) {
    if (!!flipMap[idx]) { toggleState(node); }
  });
  return flipMap.reduce(function(acc, i) { return acc + i; });
}

//Build the gameboard (static size of 50 "px")
for (i = 0; i < bh; i++) {
	for (j = 0; j < bw; j++) {
  	var block = d.createElement('div');
		var r = Math.floor(Math.random() * 16) +1
		block.setAttribute('class', 'pixel row' + j + ' col' + i + ' dead');
		block.setAttribute('style', 'background:url(img/'+r+'.png)no-repeat;');
    gameboard.appendChild(block);
  }
}

// Init
randPattern = function(size){
	var pattern = []
	for (var i = 0; i < size; i++) {
		var r = Math.floor(Math.random() * 5)
		if (r>=4) {
			pattern.push(i)
		}
	}
	return pattern
}

var randInit = randPattern(totalNodes)
var nodes = d.getElementsByClassName('pixel');
randInit.forEach(function(idx) {
	toggleState(nodes[idx]);
});

var cl = false
gameboard.onclick = function(e) {
	e = e || window.event;
	var target = e.srcElement;
  toggleState(target);
	clearInterval(isAlive);
	clearInterval(cl);
	cl = setTimeout(function () {
		run()
	}, 1000);
};

var stepGen = d.getElementById('Step');
stepGen.onclick = function() {
  generation();
}

var clearAll = d.getElementById('Clear');
clearAll.onclick = function() {
	var nodes = d.getElementsByClassName('pixel');
  [].forEach.call(nodes, function(node) {
    if (node.className.match(/live/g)) { node.className = node.className.replace('live', 'dead'); };
  });
}

var enliven = d.getElementById('Live');
var isAlive = false;
var stop = function() {
  clearInterval(isAlive);
  isAlive = false;
  enliven.textContent = "Bring To Life";
}

function run(){
	var genCount = 0;
	isAlive = setInterval(function() {
		var flipped = generation();
		if (!flipped) {
			stop();
			return;
		}
	}, 400);
}
run()
// enliven.onclick = function() {
//
// }
