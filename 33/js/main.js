var d = document,
b = d.body

// Read
var main = d.getElementsByTagName('main')[0]
var text = d.querySelector('.text-box')
var word = d.querySelector('.word-box')
var current = d.querySelector('.current-box')
var image = d.querySelector('.image-box')
var notif = true
var subCount = 0

for (var i = 0; i < pages.length; i++) {
  var lines = pages[i].lines
  var block = d.createElement('div')
  var theday = pages[i].day
  block.classList.add('day')
  block.setAttribute('id','d'+i)
  block.innerHTML += "<div class='daylabel'><a href='../"+theday+"'>day "+theday+"</a></div>"
  for (var j = 0; j < lines.length; j++) {
    var line = lines[j]
    var sub = filterSub(line.text)
    block.innerHTML += "<div class='sub'>"+sub+"</div>"
  }
  text.appendChild(block)
}
var mainCaption = d.querySelector('#et-content .captions')
function checkSentense(sub, index){
  var w = 0
  var filteredSub = filterSub(sub)
  var words = filteredSub.split(" ")
  var totalWords = words.length
  var duration = tickDuration / totalWords * 4
  if (subCount != index) {
    subCount = index
    var si = setInterval(function () {
      if (w <= totalWords) {
        if (words[w] != undefined && words[w] != "") {
          matchWord(words[w])
          var newCap = filteredSub.replace(new RegExp(words[w], 'g'), '<span class="current-word">'+words[w]+'</span>');
          mainCaption.innerHTML = newCap
        }
        w++
      }else {
        clearInterval(si)
      }
    }, duration);
  }else {
    return
  }
}

var day = 0
var lineList = []
function matchWord(currentWord){
  lineList = []
  word.innerHTML = "<span>"+currentWord+"</span>"
  for (var i = 0; i < pages.length; i++) {
    if (pages[i].day != 33) {
      var lines = pages[i].lines
      for (var j = 0; j < lines.length; j++) {
        var line = lines[j]
        var sub = filterSub(line.text)
        var words = sub.split(" ")
        for (var k = 0; k < words.length; k++) {
          if (words[k] == currentWord) {
            day = pages[i].day
            lineList.push(sub)
          }
        }

      }
    }
  }
  // console.log(lineList);
  if (lineList.length !== 0) {
    var r = Math.floor(Math.random() * lineList.length)
    var pickedLine = lineList[r]
    computeSentense(pickedLine, currentWord, day)

  }
}

function computeSentense(line, word, day){
  var newCap = line.replace(new RegExp(word, 'g'), '<span class="current-word">'+word+'</span>');
  current.innerHTML = newCap
  // Scroll to div
  day = day - 1
  var block = d.getElementById('d'+day);
  block.classList.add('selected')
  animateline()

  setTimeout(function () {
    block.classList.remove('selected')
  }, 400);
  text.scrollTo(0, block.offsetTop);
}


// drawing
let pathEl = document.querySelector('#path');
let width = 300;
let height = 300;
let buffer = height/6;
let segments = 9; // must be an odd number since we're 'curving'
function generateLine() {
  let xPoints = [];
  let yPoints = [];
  function randomize(minimum, maximum) {
    return (Math.floor(Math.random() * (maximum - minimum + 1) + minimum));
  }
  function sortNumber(a,b) {
      return a - b;
  }
  function buildPathPoints() {
    let pathD = `M${xPoints[0]},${yPoints[0]} S`;
    for (let i=1; i<yPoints.length; ++i) {
      pathD = pathD + ' ' + xPoints[i] + ',' + yPoints[i];
    }
    return pathD;
  }
  for (let i=0; i<segments; ++i) {
    xPoints.push(i*(width / (segments-1)));
    yPoints.push(randomize(buffer,height-buffer));
  }

  xPoints[0] += 10;
  xPoints[(xPoints.length - 1)] -= 10;
  pathEl.setAttribute('d', buildPathPoints());
  length = pathEl.getTotalLength();
  pathEl.style.strokeDasharray = length;
  pathEl.style.strokeDashoffset = length;
}


animateline()
function animateline(){
  generateLine();
  let length = pathEl.getTotalLength();
  pathEl.classList.remove('animate-stroke');
  pathEl.style.strokeDashoffset = length;
  console.log('reset');
  pathEl.classList.add('animate-stroke');

}
