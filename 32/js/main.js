var d = document,
b = d.body


new Vivus('face1',   {
  // type: 'oneByOne',
  type: 'sync',
    duration: 2000,
    animTimingFunction: Vivus.EASE_IN
  }
, console.log('End'));

new Vivus('face2',   {
  // type: 'oneByOne',
  type: 'sync',
  // type: 'delayed',
    duration: 2200,
    animTimingFunction: Vivus.EASE_IN
  }
, console.log('End'));

var tail = d.querySelector('.tail')
var nbr = 20
for (var i = 0; i < nbr; i++) {
  var bp = d.createElement('div')
  bp.classList.add('part')
  r = Math.PI * i* (nbr + 1)
  var delay = i* .2
  var l = i * nbr * .1
  var o =1 - i * .05
  var h =  100
  bp.setAttribute('style','margin-left:'+l+'%; margin-top:'+-l*.3+'%; opacity:'+o+';')
  tail.appendChild(bp)
}
