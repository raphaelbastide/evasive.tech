var d = document,
b = d.body

var startLine = 38
var factor = .9
var lineNbr = 14
var floor = d.querySelector('.floor')

for (var i = 0; i < lineNbr; i++) {
  startLine = Math.floor(startLine * factor)
  makeLine(startLine)
}

function makeLine(nbr){
  var line = d.createElement('div')
  h = 95 / nbr
  var dur = 100/ nbr
  line.setAttribute('style','height:'+h+'%; animation-duration:'+dur+'s;')
  line.classList.add('line')
  for (var i = 0; i < nbr; i++) {
    var b = d.createElement('button')
    b.onclick = function(){
      if (this.classList.contains('clicked')) {
        this.classList.remove('clicked')
      }else {
        this.classList.add('clicked')
      }
    }
    line.appendChild(b)
  }
  floor.appendChild(line)
}
var btns = d.querySelector('.floor button')
