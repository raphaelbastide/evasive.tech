
var d = document
var b = d.body
var list = d.querySelector('.list')
var start = new Date(2020, 03, 17)

// Date math function
Date.prototype.addDays = function(days) {
  var date = new Date(this.valueOf());
  date.setDate(date.getDate() + days);
  return date;
}

for (var i = 0; i < pages.length; i++) {
  var lines = pages[i].lines + 1
  var current = i+1
  var currentDate = start.addDays(i)
  var date = currentDate.getDate()+"/"+currentDate.getMonth()+"/"+currentDate.getFullYear()
  list.innerHTML += "<li><time>"+date+"</time><img src='home/img/"+current+".png'><a href='https://oo8.be/evasive.tech/"+current+"'>evasive.tech/"+current+"</a></li>"
}


// Mobile message
var etMobileBtn = d.querySelector(".et-mobile-notice button")
etMobileBtn.onclick = function(){
  b.classList.add('mobile-ok')
};

// about
var aboutBtn = d.querySelectorAll('.btnabout')
for (var i = 0; i < aboutBtn.length; i++) {
  aboutBtn[i].onclick = function(){
    if (b.classList.contains('about')){
      b.classList.remove('about')
    }else {
      b.classList.add('about')
    }
  }
}

// Locale
var langfr = document.querySelector('.lang.btnfr');
var langen = document.querySelector('.lang.btnen');
langfr.addEventListener('click', function(){
  if (!b.classList.contains('langfr')) {
    b.classList.remove('langen')
    b.classList.add('langfr')
  }
},false);
langen.addEventListener('click', function(){
  if (!b.classList.contains('langen')) {
    b.classList.remove('langfr')
    b.classList.add('langen')
  }
},false);
