/**
  js Cookies
 *
 */
!function(e){var n;if("function"==typeof define&&define.amd&&(define(e),n=!0),"object"==typeof exports&&(module.exports=e(),n=!0),!n){var t=window.Cookies,o=window.Cookies=e();o.noConflict=function(){return window.Cookies=t,o}}}(function(){function e(){for(var e=0,n={};e<arguments.length;e++){var t=arguments[e];for(var o in t)n[o]=t[o]}return n}function n(e){return e.replace(/(%[0-9A-Z]{2})+/g,decodeURIComponent)}return function t(o){function r(){}function i(n,t,i){if("undefined"!=typeof document){"number"==typeof(i=e({path:"/"},r.defaults,i)).expires&&(i.expires=new Date(1*new Date+864e5*i.expires)),i.expires=i.expires?i.expires.toUTCString():"";try{var c=JSON.stringify(t);/^[\{\[]/.test(c)&&(t=c)}catch(e){}t=o.write?o.write(t,n):encodeURIComponent(String(t)).replace(/%(23|24|26|2B|3A|3C|3E|3D|2F|3F|40|5B|5D|5E|60|7B|7D|7C)/g,decodeURIComponent),n=encodeURIComponent(String(n)).replace(/%(23|24|26|2B|5E|60|7C)/g,decodeURIComponent).replace(/[\(\)]/g,escape);var f="";for(var u in i)i[u]&&(f+="; "+u,!0!==i[u]&&(f+="="+i[u].split(";")[0]));return document.cookie=n+"="+t+f}}function c(e,t){if("undefined"!=typeof document){for(var r={},i=document.cookie?document.cookie.split("; "):[],c=0;c<i.length;c++){var f=i[c].split("="),u=f.slice(1).join("=");t||'"'!==u.charAt(0)||(u=u.slice(1,-1));try{var a=n(f[0]);if(u=(o.read||o)(u,a)||n(u),t)try{u=JSON.parse(u)}catch(e){}if(r[a]=u,e===a)break}catch(e){}}return e?r[e]:r}}return r.set=i,r.get=function(e){return c(e,!1)},r.getJSON=function(e){return c(e,!0)},r.remove=function(n,t){i(n,"",e(t,{expires:-1}))},r.defaults={},r.withConverter=t,r}(function(){})});
//# sourceMappingURL=/sm/b0ce608ffc029736e9ac80a8dd6a7db2da8e1d45d2dcfc92043deb2214aa30d8.map

var d = document
var b = d.body
var tickDuration = 1000
var currentTick = 0
var currentSub = 0
var subTimer = 0
var currentPage = Number(d.title.split("/")[1]) - 1 // zero based

var loop = setInterval(function(){
  checkSub()
}, tickDuration);

function checkSub(){
  var subs = pages[currentPage].lines
  var totalSub = subs.length
  var subDuration = subs[currentSub].duration;
  var subText = subs[currentSub].text;
  if (subTimer === 0) { // first tick width sub
    displaySub(subText, currentSub)
    subTimer++
  }
 if (subTimer < subDuration) { // keep it, increment
    subTimer++
  }else {
    currentSub++
    subTimer = 0
  }

  if (currentSub >= totalSub) {   // end
    currentSub = 0 // / loop
  }
  currentTick++
}

var etcontent = d.createElement("aside")
etcontent.setAttribute('id', 'et-content');

d.body.append(etcontent);
etcontent.innerHTML += '<button title="Navigation" id="et-button"><span class="helper">?</span></button>';
etcontent.innerHTML += '<div id="et-menu"><div class="et-nav"></div><label><input id="captionBtn" type="checkbox"></input> Show captions</label></div>';
etcontent.innerHTML += '<div class="captions"></div>'

// Cookies.set('et-menu-helper', true);
var etbutton = d.getElementById("et-button")
etbutton.onclick = function(){
  Cookies.set('et-menu-helper', false);
  setMenuHelper(false)
  if (b.getAttribute('data-etmenu') == 'true') {
    b.setAttribute('data-etmenu', 'false')
  }else {
    b.setAttribute('data-etmenu', 'true')
  }
};

var captionBtn = d.getElementById("captionBtn")
setCaption(true)
// Check captions mode in cookies
var co = Cookies.get('et-captions');
if (co === 'false') {
  setCaption(false)
}else if (co == 'true' || co === undefined) {
  setCaption(true)
}
// Check if menu helper should be shown in cookies
var mh = Cookies.get('et-menu-helper');
if (mh === 'false') {
  setMenuHelper(false)
}else if (mh == 'true' || mh === undefined) {
  setMenuHelper(true)
}

function setCaption(mode){
  if (mode == true) {
    var captionsOn = true
    captionBtn.checked = true
    b.setAttribute('data-captions', 'true')
  }else {
    var captionsOn = false
    captionBtn.checked = false
    b.setAttribute('data-captions', 'false')
  }
}
function setMenuHelper(mode){
  if (mode == true) {
    Cookies.set('et-menu-helper', true);
    b.setAttribute('data-menu-helper', 'true')
  }else {
    Cookies.set('et-menu-helper', false);
    b.setAttribute('data-menu-helper', 'false')
  }
}

captionBtn.addEventListener('change', (event) => {
  if (event.target.checked) {
    captionBtn.checked = true
    Cookies.set('et-captions', true);
    b.setAttribute('data-captions', 'true')
  } else {
    captionBtn.checked = false
    Cookies.set('et-captions', false);
    b.setAttribute('data-captions', 'false')
  }
})

var list = document.querySelector('.et-nav')
appendData(pages);
function appendData(data) {
  var section = document.createElement("section");
  if (typeof pages[currentPage - 1] !== "undefined" ) {
    var prev = currentPage // zero based
    section.innerHTML += '<a href="../'+prev+'/">< Day '+prev+'</a>'
  }
  section.innerHTML += '<a href="../">HOME</a>'
  if (typeof pages[currentPage + 1] !== "undefined" ) {
    var next = currentPage + 2 // zero based
    section.innerHTML += '<a href="../'+next+'/">Day '+next+' ></a>'
  }
  list.appendChild(section);
}

function displaySub(sub, index){
  var captionsContainers = d.querySelectorAll('.captions')
  for (var i = 0; i < captionsContainers.length; i++) {
    captionsContainers[i].innerHTML = '<span class="cap cap-'+index+'">'+filterSub(sub)+'</span>'
  }
  if (typeof notif != 'undefined') { // used in 33
    checkSentense(sub, index)
  }
}
function filterSub(sub){
  var filteredSub = sub.replace(/[_]/g,' ');
  return filteredSub
}
